<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Table(name="PostCodes")
 * @ORM\Entity(repositoryClass="App\Repository\PostcodeRepository")
 */
class Postcode implements \JsonSerializable
{
    /**
     * @ORM\Id()
     * @ORM\Column(name="PostCode",type="string")
     */
    private $postcode;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $statename;

      /**
     * @ORM\Column(type="string", length=255)
     */
    private $town;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Customer", mappedBy="postcode")
     */
    private $customers;

    

    /**
     * Get the value of postcode
     */ 
    public function getPostcode()
    {
        return $this->postcode;
    }

    /**
     * Set the value of postcode
     *
     * @return  self
     */ 
    public function setPostcode($postcode)
    {
        $this->postcode = $postcode;

        return $this;
    }

    /**
     * Get the value of statename
     */ 
    public function getStatename()
    {
        return $this->statename;
    }

    /**
     * Set the value of statename
     *
     * @return  self
     */ 
    public function setStatename($statename)
    {
        $this->statename = $statename;

        return $this;
    }

    /**
     * Get the value of town
     */ 
    public function getTown()
    {
        return $this->town;
    }

    /**
     * Set the value of town
     *
     * @return  self
     */ 
    public function setTown($town)
    {
        $this->town = $town;

        return $this;
    }

    public function jsonSerialize(): array
    {
        return [
            'postcode'      => $this->postcode,
            'town'     => $this->town,
            'statename'     => $this->statename
        ];
    }

}
