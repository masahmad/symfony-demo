<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Table(name="ConsMf")
 * @ORM\Entity(repositoryClass="App\Repository\CustomerRepository")
 */
class Customer implements \JsonSerializable
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="Name", type="string", length=250, unique=true)
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="AccNo", type="string", length=14)
     */
    private $accno;

    /**
     * @var string
     *
     * @ORM\Column(name="Addr1", type="string", length=255, unique=true)
     */
    private $address1;

    /**
     * @var string
     *
     * @ORM\Column(name="Addr2", type="string", length=255)
     */
    private $address2;


    /**
     * @var string
     *
     * @ORM\Column(name="Phone", type="string", length=255, nullable=true)
     */
    private $phone;

    /**
     * @var string
     *
     * @ORM\Column(name="AccBalance", type="decimal", precision=10, scale=2)
     */
    private $accountBalance;


     /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Postcode", inversedBy="customers"))
     * @ORM\JoinColumn(name="Postcode", referencedColumnName="PostCode")
     */
    private $postcode;

   

    public function getId(): ?int
    {
        return $this->id;
    }

   

    /**
     * Get the value of accountBalance
     *
     * @return  string
     */ 
    public function getAccountBalance()
    {
        return $this->accountBalance;
    }

    /**
     * Set the value of accountBalance
     *
     * @param  string  $accountBalance
     *
     * @return  self
     */ 
    public function setAccountBalance(string $accountBalance)
    {
        $this->accountBalance = $accountBalance;

        return $this;
    }

    /**
     * Get the value of phone
     *
     * @return  string
     */ 
    public function getPhone()
    {
        return $this->phone;
    }

    /**
     * Set the value of phone
     *
     * @param  string  $phone
     *
     * @return  self
     */ 
    public function setPhone(string $phone)
    {
        $this->phone = $phone;

        return $this;
    }

   
    /**
     * Get the value of name
     *
     * @return  string
     */ 
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set the value of name
     *
     * @param  string  $name
     *
     * @return  self
     */ 
    public function setName(string $name)
    {
        $this->name = $name;

        return $this;
    }

    public function jsonSerialize(): array
    {
        return [
            'name'      => $this->name,
            'accno'     => $this->accno,
            'addr1'     => $this->address1,
            'addr2'     => $this->address2,
            'postcode'  => $this->postcode,
            'phone'  => $this->phone,
            'accBal'  => $this->accountBalance
        ];
    }

    /**
     * Get the value of town
     */ 
    public function getTown()
    {
        return $this->town;
    }

    /**
     * Set the value of town
     *
     * @return  self
     */ 
    public function setTown($town)
    {
        $this->town = $town;

        return $this;
    }

    /**
     * Get the value of address1
     *
     * @return  string
     */ 
    public function getAddress1()
    {
        return $this->address1;
    }

    /**
     * Set the value of address1
     *
     * @param  string  $address1
     *
     * @return  self
     */ 
    public function setAddress1(string $address1)
    {
        $this->address1 = $address1;

        return $this;
    }

    /**
     * Get the value of address2
     *
     * @return  string
     */ 
    public function getAddress2()
    {
        return $this->address2;
    }

    /**
     * Set the value of address2
     *
     * @param  string  $address2
     *
     * @return  self
     */ 
    public function setAddress2(string $address2)
    {
        $this->address2 = $address2;

        return $this;
    }

    /**
     * Get the value of accno
     *
     * @return  string
     */ 
    public function getAccno()
    {
        return $this->accno;
    }

    /**
     * Set the value of accno
     *
     * @param  string  $accno
     *
     * @return  self
     */ 
    public function setAccno(string $accno)
    {
        $this->accno = $accno;

        return $this;
    }

    /**
     * Get the value of postcode
     */ 
    public function getPostcode()
    {
        return $this->postcode;
    }

    /**
     * Set the value of postcode
     *
     * @return  self
     */ 
    public function setPostcode($postcode)
    {
        $this->postcode = $postcode;

        return $this;
    }
}
