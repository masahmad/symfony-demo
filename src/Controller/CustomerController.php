<?php

namespace App\Controller;

use App\Entity\Customer;
use App\Repository\CustomerRepository;
use App\Repository\PostcodeRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/customer")
 */
class CustomerController extends AbstractController
{
    /**
     * @Route("/", name="customer_index", methods={"GET"})
     */
    public function index(CustomerRepository $customerRepository): Response
    {
        return $this->render('customer/index.html.twig', [
            //'articles' => $customerRepository->findAll(),
        ]);
    }


    /**
     * @Route("/api/list", name="customer_list", methods={"GET","POST"})
     */
    public function list(CustomerRepository $customerRepository, Request $request): Response
    {
        $limit = $request->get('limit');
        $limit = $limit == NULL ? 50 : $limit;

        $customers = $customerRepository->list($limit);
        $response = new JsonResponse($customers,JsonResponse::HTTP_OK);
        return $response;
    }

     /**
     * @Route("/api/show/{id}", name="customer_show", methods={"GET","POST"})
     */
    public function show(CustomerRepository $customerRepository, $id): Response
    {
        $customer = $customerRepository->find($id);
        $response = new JsonResponse($customer,JsonResponse::HTTP_OK);
        return $response;
    }

    /**
     * @Route("/api/show/accno/{accno}", name="customer_accno", methods={"GET","POST"})
     */
    public function showByAccountNo(CustomerRepository $customerRepository, $accno): Response
    {
        $customer = $customerRepository->findOneBy(array('accno' => $accno));
        $response = new JsonResponse($customer,JsonResponse::HTTP_OK);
        return $response;
    }

     /**
     * @Route("/api/new", name="customer_new", methods={"POST"})
     */
    public function newCustomer(CustomerRepository $customerRepository, PostcodeRepository $postcodeRepository, Request $request): Response
    {
        $customer = new Customer();
        
        $entityManager = $this->getDoctrine()->getManager();

        $custname = $request->get('custname');
        $address1 = $request->get('address1');
        $address2 = $request->get('address2');
        $postcode = $postcodeRepository->find($request->get('postcode'));


        $customer->setName($custname);
        $customer->setAddress1($address1);
        $customer->setAddress2($address2);
        $customer->setPostcode($postcode);

        $entityManager->persist($customer);
        $entityManager->flush();

        
        // Generate new account number
        $accno = '059'.str_pad($customer->getId(),11,'0',STR_PAD_LEFT);
        $customer->setAccNo($accno);
        $entityManager->flush();

        $response = new JsonResponse(array('isSuccess' => true, 'msg' => 'Update successful!'),JsonResponse::HTTP_OK);   
        
        
        
        return $response;
    }

    /**
     * @Route("/api/update/accno/{accno}", name="customer_update_accno", methods={"POST"})
     */
    public function updateCustomerInfo(CustomerRepository $customerRepository, PostcodeRepository $postcodeRepository, Request $request, $accno): Response
    {
        $customer = $customerRepository->findOneBy(array('accno' => $accno));
        
        if(!$customer) {
            $response = new JsonResponse(array('isSuccess' => false, 'msg' => 'Invalid account number!'),JsonResponse::HTTP_OK);   
        } else {

            $entityManager = $this->getDoctrine()->getManager();

            $custname = $request->get('custname');
            $address1 = $request->get('address1');
            $address2 = $request->get('address2');
            $postcode =  $postcodeRepository->find($request->get('postcode'));


            $customer->setName($custname);
            $customer->setAddress1($address1);
            $customer->setAddress2($address2);
            $customer->setPostcode($postcode);

            $entityManager->flush();

            $response = new JsonResponse(array('isSuccess' => true, 'msg' => 'Update successful!'),JsonResponse::HTTP_OK);   
        }
        
        
        return $response;
    }

    /**
     * @Route("/api/delete/accno/{accno}", name="customer_delete_accno", methods={"POST"})
     */
    public function deleteCustomer(CustomerRepository $customerRepository, Request $request, $accno): Response
    {
        $customer = $customerRepository->findOneBy(array('accno' => $accno));
        
        if(!$customer) {
            $response = new JsonResponse(array('isSuccess' => false, 'msg' => 'Invalid account number!'),JsonResponse::HTTP_OK);   
        } else {

            $entityManager = $this->getDoctrine()->getManager();

            $entityManager->remove($customer);
            $entityManager->flush();

            $response = new JsonResponse(array('isSuccess' => true, 'msg' => 'Delete successful!'),JsonResponse::HTTP_OK);   
        }
        
        
        return $response;
    }

    
}
