<?php

namespace App\Controller;

use App\Entity\Postcode;
use App\Repository\PostcodeRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/postcode")
 */
class PostcodeController extends AbstractController
{
    /**
     * @Route("/", name="postcode_index", methods={"GET"})
     */
    public function index(PostcodeRepository $postcodeRepository): Response
    {
        return $this->render('postcode.html.twig');
    }


    /**
     * @Route("/api/list", name="postcode_list", methods={"GET","POST"})
     */
    public function list(PostcodeRepository $postcodeRepository, Request $request): Response
    {
        $limit = $request->get('limit');
        $limit = $limit == NULL ? 50 : $limit;

        $postcodes = $postcodeRepository->list($limit);
        $response = new JsonResponse($postcodes,JsonResponse::HTTP_OK);
        return $response;
    }

    /**
     * @Route("/api/list/search", name="postcode_listsearch", methods={"GET","POST"})
     */
    public function listSearch(PostcodeRepository $postcodeRepository, Request $request): Response
    {
        $limit = $request->get('limit');
        $limit = $limit == NULL ? 50 : $limit;

        $searchTerm  = $request->get('search');
        
        if($searchTerm != NULL) {
            $postcodes = $postcodeRepository->listSearch($searchTerm,$limit);
            $response = new JsonResponse($postcodes,JsonResponse::HTTP_OK);
        } else {
            $response = new JsonResponse(array(),JsonResponse::HTTP_OK);
        }

        
        return $response;
    }

     /**
     * @Route("/api/show/{code}", name="postcode_show", methods={"GET","POST"})
     */
    public function show(PostcodeRepository $postcodeRepository, $code): Response
    {
        $postcodes = $postcodeRepository->find($code);
        $response = new JsonResponse($postcodes,JsonResponse::HTTP_OK);
        return $response;
    }

   
    
}
