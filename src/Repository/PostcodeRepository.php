<?php

namespace App\Repository;

use App\Entity\Postcode;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method Postcode|null find($id, $lockMode = null, $lockVersion = null)
 * @method Postcode|null findOneBy(array $criteria, array $orderBy = null)
 * @method Postcode[]    findAll()
 * @method Postcode[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class PostcodeRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, Postcode::class);
    }

    // /**
    //  * @return Customer[] Returns an array of Author objects
    //  */
    
    public function list($limit)
    {
        return $this->createQueryBuilder('a') 
            ->select('a.postcode, a.town, a.statename')                    
            ->orderBy('a.postcode', 'ASC')
            ->setMaxResults($limit)
            ->getQuery()
            ->getResult()
        ;
    }

    public function listSearch($searchTerm,$limit)
    {
        return $this->createQueryBuilder('a') 
            ->select('a.postcode AS id, a.town AS text')
            ->where('a.postcode LIKE :term')
            ->orderBy('a.postcode', 'ASC')
            ->setParameter('term', $searchTerm.'%')
            ->setMaxResults($limit)
            ->getQuery()
            ->getResult()
        ;
    }
    

    /*
    public function findOneBySomeField($value): ?Author
    {
        return $this->createQueryBuilder('a')
            ->andWhere('a.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
